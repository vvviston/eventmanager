package cz.komix.knetl.eventmanager.dao;

import cz.komix.knetl.eventmanager.exception.CRUDException;
import cz.komix.knetl.eventmanager.model.Event;

import java.util.Date;

/**
 * CRUD operations for a Event
 */
public interface EventDao {

    /**
     *  Save an new Event.
     *
     * @Param event - event to be stored
     * @return Unique ID of an new event
     * @throws CRUDException if saving new event failed
     *
     * */
    String createEvent(final Event newEvent) throws CRUDException;

    /**
     *  Permanently delete an Event
     *
     * @Param rowKey - Unique event ID
     * @return Integer http status of operation
     * @throws CRUDException if removing event failed
     * */
    void deleteEvent (final String rowKey) throws CRUDException;

    /**
     *  Returns events by a date.
     *
     * @Param date - searching parameter
     * @return Collection of events start that date
     * @throws CRUDException if listing events failed
     * */
    Iterable<Event> findByDate(final Date date) throws CRUDException;

    /**
     *  Returns events by a month.
     *
     * @Param date - searching parameter
     * @return Collection of events start that date
     * @throws CRUDException if listing events failed
     * */
    Iterable<Event> findByMonth(final Date date) throws CRUDException;
}
