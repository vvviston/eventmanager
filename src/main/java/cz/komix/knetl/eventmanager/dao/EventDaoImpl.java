package cz.komix.knetl.eventmanager.dao;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.*;
import cz.komix.knetl.eventmanager.EventDto.EventDto;
import cz.komix.knetl.eventmanager.exception.CRUDException;
import cz.komix.knetl.eventmanager.mapper.EventMapper;
import cz.komix.knetl.eventmanager.model.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class EventDaoImpl implements EventDao{

    private final String PARTITION_KEY = "PartitionKey";
    private final String ROW_KEY = "RowKey";
    private final String DAY_KEY = "DayKey";

    @Autowired
    private CloudStorageAccount cloudStorageAccount;


    /**
     *  Save an new Event.
     *
     * @Param event - event to be stored
     * @return Unique ID of an new event
     * @throws CRUDException if saving new event failed
     *
     * */
    @Override
    public String createEvent(final Event newEvent) throws CRUDException {
        CloudTableClient tableClient = cloudStorageAccount.createCloudTableClient();
        try {
            CloudTable cloudTable = tableClient.getTableReference(Event.TABLE_NAME);
            TableOperation insertEvent = TableOperation.insertOrReplace(newEvent);
            TableResult result = cloudTable.execute(insertEvent);
            if (result.getHttpStatusCode() > 199 && result.getHttpStatusCode() < 300) {
                log.info("New event succesfully created: " + newEvent.getRowKey());
                return newEvent.getRowKey();
            }
            log.error("Creating and event error. Http status code: " + result.getHttpStatusCode());
            throw new CRUDException("HTTP status code: " + result.getHttpStatusCode());
        } catch (URISyntaxException | StorageException e) {
            e.printStackTrace();
            throw new CRUDException();
        }
    }

    /**
     *  Permanently delete an Event
     *
     * @Param rowKey - Unique event ID
     * @return Integer http status of operation
     * @throws CRUDException if removing event failed
     * */
    @Override
    public void deleteEvent(final String rowKey) throws CRUDException {
        CloudTableClient tableClient = cloudStorageAccount.createCloudTableClient();
        try {
            CloudTable cloudTable = tableClient.getTableReference(Event.TABLE_NAME);
            String rowFilter = TableQuery.generateFilterCondition(ROW_KEY, TableQuery.QueryComparisons.EQUAL, rowKey);
            TableQuery<Event> rowQuery = TableQuery.from(Event.class).where(rowFilter);

            Iterable<Event> eventsToRemove = cloudTable.execute(rowQuery);
            for (Event event: eventsToRemove) {
                TableOperation deleteEvent = TableOperation.delete(event);
                final TableResult result = cloudTable.execute(deleteEvent);
                if (result.getHttpStatusCode() < 200 && result.getHttpStatusCode() > 299){
                    throw new CRUDException("HTTP status code: " + result.getHttpStatusCode());
                }
            }
        } catch (URISyntaxException | StorageException e) {
            log.error("Removing event by date error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *  Returns events by a date.
     *
     * @Param date - searching parameter
     * @return Collection of events start that date
     * @throws CRUDException if listing events failed
     * */
    @Override
    public Iterable<Event> findByDate(final Date date) throws CRUDException {
        CloudTableClient tableClient = cloudStorageAccount.createCloudTableClient();
        try {
            CloudTable cloudTable = tableClient.getTableReference(Event.TABLE_NAME);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");
            String partitionFilter = TableQuery.generateFilterCondition(PARTITION_KEY, TableQuery.QueryComparisons.EQUAL, dateFormat.format(date));

            SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
            String rowFilter = TableQuery.generateFilterCondition(DAY_KEY, TableQuery.QueryComparisons.EQUAL, dayFormat.format(date));

            String combinedFilter = TableQuery.combineFilters(partitionFilter, TableQuery.Operators.AND, rowFilter);
            TableQuery<Event> partitionQuery = TableQuery.from(Event.class).where(combinedFilter);

            List<EventDto> events = new ArrayList<>();
            cloudTable.execute(partitionQuery).forEach(e -> events.add(EventMapper.convert(e)));
            return cloudTable.execute(partitionQuery);

        } catch (URISyntaxException | StorageException e) {
            log.error("Removing event by date error: " + e.getMessage());
            throw new CRUDException();
        }
    }

    /**
     *  Returns events by a month.
     *
     *  @Param date - searching parameter. Day is omitted.
     *  @return List of events
     *  @throws CRUDException if listing events failed
     * */
    @Override
    public Iterable<Event> findByMonth(final Date date) throws CRUDException {
        CloudTableClient tableClient = cloudStorageAccount.createCloudTableClient();
        try {
            CloudTable cloudTable = tableClient.getTableReference(Event.TABLE_NAME);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-yyyy");
            String partitionFilter = TableQuery.generateFilterCondition(PARTITION_KEY, TableQuery.QueryComparisons.EQUAL, dateFormat.format(date));

            TableQuery<Event> partitionQuery = TableQuery.from(Event.class).where(partitionFilter);
            return cloudTable.execute(partitionQuery);
        } catch (URISyntaxException | StorageException e) {
            log.error("Listing events by date error: " + e.getMessage());
            throw new CRUDException(e.getMessage());
        }
    }
}
