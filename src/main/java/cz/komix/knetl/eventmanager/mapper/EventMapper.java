package cz.komix.knetl.eventmanager.mapper;

import cz.komix.knetl.eventmanager.EventDto.EventDto;
import cz.komix.knetl.eventmanager.model.Event;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 *
 *  Map a Event model object to EventDto API representation and vice versa
 *
 * */
public class EventMapper {
    public static Event convert(final EventDto eventDto) {
        Event event = new Event(
                eventDto.getName(),
                eventDto.getDescription(),
                eventDto.getStart().getTime(),
                eventDto.getEnd().getTime()
        );
        if (!StringUtils.isEmpty(eventDto.getRowKey())) {
            event.setRowKey(eventDto.getRowKey());
        }
        return event;
    }

    public static EventDto convert(final Event event) {
        EventDto eventDto = new EventDto(
                event.getName(),
                event.getDescription(),
                new Date(event.getStartTimestamp()),
                new Date(event.getEndTimestamp())
        );
        eventDto.setRowKey(event.getRowKey());
        return eventDto;
    }
}
