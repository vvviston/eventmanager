package cz.komix.knetl.eventmanager.EventDto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 *  The EventDto represents the Event model object in REST API
 * */
@Data
public class EventDto implements Comparable<EventDto>{

    private String rowKey;

    @NotBlank(message = "Name is mandatory!")
    private String name;
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss")
    @NotNull
    private Date start;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss")
    @NotNull
    private Date end;

    //default constructor
    public EventDto(){}

    public EventDto(String name, Date start, Date end){
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public EventDto(String name, String description, Date start, Date end){
        this(name, start, end);
        this.description = description;
    }


    @Override
    public int compareTo(EventDto o) {
        return this.getStart().compareTo(o.getStart());
    }
}
