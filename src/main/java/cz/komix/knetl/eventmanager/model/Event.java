package cz.komix.knetl.eventmanager.model;

import com.microsoft.azure.storage.table.TableServiceEntity;
import cz.komix.knetl.eventmanager.util.UUIDGenerator;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 *  The Event business object represents a calendar event for Azure Table Storage
 *
 *  partitionKey - the first key is a String that is represent as a date in format MM-yyyy
 *  rowKey - the second key is a String that is represent as 21 digits unique number ID
 *
 * */

@Data
public class Event extends TableServiceEntity {

    public static final String TABLE_NAME = "EventTable";

    private String name;
    private String description;
    private Long startTimestamp;
    private Long endTimestamp;
    private String dayKey;

    //default constructor
    public Event() {}

    public Event(String name, Long startTimestamp, Long endTimestamp) {
        this.name = name;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.partitionKey = new SimpleDateFormat("MM-yyyy").format(new Date(startTimestamp));
        this.dayKey = new SimpleDateFormat("dd").format(new Date(startTimestamp));
        this.rowKey = UUIDGenerator.getNext();
    }

    public Event(String name, String description, Long startTimestamp, Long endTimestamp) {
        this(name, startTimestamp, endTimestamp);
        this.description = description;
    }

}
