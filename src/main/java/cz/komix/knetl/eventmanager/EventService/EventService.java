package cz.komix.knetl.eventmanager.EventService;

import cz.komix.knetl.eventmanager.EventDto.EventDto;
import cz.komix.knetl.eventmanager.exception.CRUDException;

import java.util.Date;
import java.util.List;

/**
 * CRUD operations for a Event model object
 */
public interface EventService {

    /**
     * Save an new Event.
     *
     * @return Unique ID of an new event
     * @throws CRUDException if saving new event failed
     */
    String create(final EventDto eventDto);

    /**
     *  Permanently delete an Event
     *
     * @return Status - The event is removed when status is true.
     *
     * */
    boolean permanentlyDelete(final String rowKey);

    /**
     *  Find events by a date.
     *
     * @return Collection of result events. Null if listing events failed
     * */
    List<EventDto> listEventsByDate(final Date date);

    /**
     *  Find events by a month.
     *
     *  @return List of events. Null if listing events failed
     * */
    List<EventDto> listEventsByMonth(final Date date);
}
