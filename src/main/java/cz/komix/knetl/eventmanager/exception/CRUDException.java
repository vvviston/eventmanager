package cz.komix.knetl.eventmanager.exception;

/**
 *  Main exception for CRUD operation
 * */
public class CRUDException extends Exception {

    public CRUDException() {}

    public CRUDException(String message) {
        super(message);
    }
}
