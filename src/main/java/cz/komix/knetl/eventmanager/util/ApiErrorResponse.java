package cz.komix.knetl.eventmanager.util;

import lombok.Data;

/**
 *  The ApiErrorResponse represents error response in REST API
 * */
@Data
public class ApiErrorResponse {

    private String status = "ERROR";
    private String message;

    //default constructor
    public ApiErrorResponse(){}

    public ApiErrorResponse(String message){
        this.message = message;
    }

}
