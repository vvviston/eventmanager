package cz.komix.knetl.eventmanager.EventService;

import cz.komix.knetl.eventmanager.EventDto.EventDto;
import cz.komix.knetl.eventmanager.dao.EventDao;
import cz.komix.knetl.eventmanager.exception.CRUDException;
import cz.komix.knetl.eventmanager.mapper.EventMapper;
import cz.komix.knetl.eventmanager.model.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * CRUD operations for a Event model object
 */
@Service
@Slf4j
public class EventServiceImpl implements EventService {

    @Autowired
    private EventDao eventDao;

    /**
     * Save an new Event.
     *
     * @return Unique ID of an new event
     * @throws CRUDException if saving new event failed
     */
    @Override
    public String create(final EventDto eventDto) {
        Event event = EventMapper.convert(eventDto);
        try {
            return  eventDao.createEvent(event);
        } catch (CRUDException e) {
            log.error("Creating an event error. " + e.getMessage());
            return null;
        }
    }

    /**
     *  Permanently delete an Event
     *
     * @return Status - The event is removed when status is true.
     *
     * */
    @Override
    public boolean permanentlyDelete(final String rowKey) {
        try {
            eventDao.deleteEvent(rowKey);
            return true;
        } catch (CRUDException e) {
            log.error("Removing an event error. " + e.getMessage());
            return false;
        }
    }

    /**
     *  Find events by a date.
     *
     * @return Collection of result events. Null if listing events failed
     * */
    @Override
    public List<EventDto> listEventsByDate(final Date date) {
        try {
            Iterable<Event> result = eventDao.findByDate(date);
            List<EventDto> events = new ArrayList<>();
            result.forEach(e -> events.add(EventMapper.convert(e)));
            Collections.sort(events);
            return events;
        } catch (CRUDException e) {
            log.error("Listing events error. " + e.getMessage());
            return null;
        }
    }

    /**
     *  Find events by a month.
     *
     *  @return List of events. Null if listing events failed
     * */
    @Override
    public List<EventDto> listEventsByMonth(final Date date) {
        try {
            Iterable<Event> result = eventDao.findByMonth(date);
            List<EventDto> events = new ArrayList<>();
            result.forEach(e -> events.add(EventMapper.convert(e)));
            Collections.sort(events);
            return events;
        } catch (CRUDException e) {
            log.error("Listing events error. " + e.getMessage());
            return null;
        }
    }
}
