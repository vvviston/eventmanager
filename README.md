# README #

### What is this repository for? ###

Interview project for KOMIX called EventManager.
It is simple REST-API Spring boot application which 
saves the data in the Microsoft Azure Storage Table.

### How do I get set up? ###

Clone the repository
Open in your favourite IDE
Install Lombok plugin in your IDE
Run the application from the command line with following command: 
mvn spring-boot:run

### Contribution guidelines ###

The methods are commented using javadoc
There are basic tests on each CRUD operation
I used Swagger to generate simple UI which helps you using the app without SOAP UI, Postman etc.
