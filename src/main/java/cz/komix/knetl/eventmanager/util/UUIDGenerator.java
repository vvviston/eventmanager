package cz.komix.knetl.eventmanager.util;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;

public class UUIDGenerator {

    private static SecureRandom RANDOM = null;

    private UUIDGenerator(){}

    /*
     * Generate 21 digits number ID
     * */
    public static String getNext(){
        final Long timestamp = new Date().getTime();
        final SecureRandom random = getRandom();
        final BigInteger randomNumber = BigInteger.valueOf(10000000L + random.nextInt(90000000));
        final StringBuilder sb = new StringBuilder();
        sb.append(randomNumber).append(timestamp);
        return sb.toString();
    }

    private static SecureRandom getRandom(){
        if(RANDOM == null) {
            RANDOM = new SecureRandom();
        } return RANDOM;
    }
}
